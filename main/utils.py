def handler(rule, symbol):
    rule = list(rule)
    exp = ["", symbol, ""]
  
    for i in [1, 3]:
        if rule[i].name == "var":
            exp[i-1] = rule[i].text
        else:
            exp[i-1] = rule_handler(rule[i])

    return " ".join(exp)

def rule_handler(rule):
    if rule.name == "disj": # or
        return handler(rule, "||")
    elif rule.name == "conj": # and
        return handler(rule, "&&")
    elif rule.name == "imp": # implies
        return handler(rule, "->")
    elif rule.name == "eq": # iff
        return handler(rule, "<->")
    else: # not
        return "!{}".format(rule.find("var").text)

# Get constraints
def constraint(constraints):
    cons = {}
    
    for constraint in constraints:
        if constraint.name is None:
            continue
        
        for rule in constraint:
            if rule.name is None:
                continue
            
            var = list(rule)
            
            if var[3].name == "not":
                cons[var[1].text] = {
                    "constraint": "exclude"
                }
            else:
                cons[var[1].text] = {
                    "constraint": "require"
                }

            cons[var[1].text]["value"] = var[3].text.strip()
    
    return cons

def is_mandatory(element):
    try:
        element['mandatory']
        return True
    except KeyError:
        return False

def is_all_children_optional(elements):
    return elements.name == "and" and len([e for e in elements if e.name is not None and is_mandatory(e)]) == 0

def get_cardinality(el_name):
    if el_name == "and":
        return "allof"
    elif el_name == "alt":
        return "oneof"
    elif el_name == "or":
        return "[1..*]"
    else:
        return ""

def convert_datatype(dt):
    if dt == "long":
        return "Int"
    elif dt == "boolean":
        return "Bool"
    else:
        return dt

def get_group_type(el, is_mandatory):

    if el.name == 'and' and str.lower(is_mandatory) == 'false' and is_all_children_optional(el):
        return "[0..*]"
    elif el.name == 'and' and str.lower(is_mandatory) == 'true' and is_all_children_optional(el):
        return "[1..*]"
    else:
        return get_cardinality(el.name)