from bs4 import BeautifulSoup
from feature_diagram_parser import parse_root
from abs_files_constructor import construct_feature
from utils import constraint
from jinja2 import Environment, FileSystemLoader

import utils
import json
import sys

FILE_PATH = sys.argv[1]

file = open(FILE_PATH, 'r+')
filename = FILE_PATH.split('.')[0]

# Parse to dictionary

soup = BeautifulSoup(file.read(), 'lxml')

root = list(soup.find('struct'))[1]
constraints = soup.find('constraints')

cons = constraint(constraints)

dct = parse_root(root, cons)

json_file = open(f"dictionary_output/{filename}.json", 'w+')
json_file.write(
    json.dumps(
        dct, indent=2
    )
)
json_file.close()


# Template

file_loader = FileSystemLoader('template')
env = Environment(loader=file_loader)

abs_file = open(f"output/{filename}.abs", 'w+')
abs_file.write(
    construct_feature(
        env,
        dct,
        0
    )[:-2:]
)
abs_file.close()
