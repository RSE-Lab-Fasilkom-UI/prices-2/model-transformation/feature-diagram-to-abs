def construct_attribute(env, attribute, indent):
    template = env.get_template('attribute.jinja')
    
    return template.render(
        type=attribute['type'],
        name=attribute['name'],
        indent=indent
    )

def construct_constraint(env, constraint, indent):
    template = env.get_template('constraint.jinja')
    
    return template.render(
        type=constraint['type'],
        value=constraint['value'],
        indent=indent
    )

def construct_feature(env, feature, indent):
    template = env.get_template('feature.jinja')
    
    opt = ""
    try:
        opt = '' if feature['mandatory'] == "true" else "opt "
    except KeyError:
        opt = ''
    
    features = ''
    attributes = ''
    if 'features' in feature and feature['features'] != []:
        for feature_item in feature['features']:
            features += "{}".format(
                construct_feature(env, feature_item, indent+2)
            )
        try:
            return template.render(
                opt=opt,
                feature_name=feature['name'],
                features=features[:-2:],
                group_type=feature['group_type'],
                has_features=True,
                indent=indent
            )
        except KeyError:
            return template.render(
                opt="root ",
                feature_name=feature['root'],
                features=features[:-2:],
                group_type=feature['group_type'],
                has_features=True,
                indent=indent
            )
 
    elif 'attributes' in feature and feature['attributes'] != []:
        for attr in feature['attributes']:
            attributes += "{}\n".format(
                construct_attribute(env, attr, indent+1)
            )
            
        return template.render(
            opt=opt,
            feature_name=feature['name'],
            attributes=attributes[:-1:],
            has_attributes=True,
            indent=indent
        )
    elif 'constraint' in feature:
        return template.render(
            opt=opt,
            feature_name=feature['name'],
            constraint=construct_constraint(env, feature['constraint'], indent+1),
            has_constraint=True,
            indent=indent
        )
    else:
        return template.render(
            opt=opt,
            feature_name=feature['name'],
            indent=indent
        )