from bs4 import BeautifulSoup
import utils

def parse_root(root, constraints):
    
    feature = {
        "root": root['name'],
        "mandatory": root['mandatory'],
        "group_type": utils.get_cardinality(root.name),
        "features": []
    }
    
    for el in root:
        
        if el.name is None:
            continue
            
        feature["features"].append(parse_features(el, constraints, feature["group_type"]))

    return feature

def parse_features(el, constraints, parent_cardinality = ""):
    if el.name is None:
        return
    
    feature = {
        "name": el['name'],
        "group_type": "",
        "mandatory": "",
        "features": [],
        "attributes": []
    }

    # Set mandatory/optional
    if el.name is not "allof":
        try:
            feature['mandatory'] = str(el['mandatory'])
        except KeyError:
            feature['mandatory'] = str(False)

    # Set features
    is_group = True if ((el.name in ["and", "alt", "or"]) or (el.find("attribute") is not None)) else False
    # print(el.name, el['name'], is_group)
    
    if is_group:

        # Set group_type
        feature['group_type'] = utils.get_group_type(el, feature['mandatory'])

        for elem in el:
            
            if elem.name is None:
                continue

            # Recursive
            if el.name in ["and", "alt", "or"]:
                feature['features'].append(parse_features(elem, constraints, feature['group_type']))
                
            elif el.name == "feature":
                try:
                    del feature['features']
                except KeyError:
                    pass
                
                feature['attributes'].append(parse_features(elem, constraints, feature['group_type']))
            else:
                pass
    else:
        try:
            del feature['features']
            del feature['group_type']
        except KeyError:
            pass
        
        if el['name'] in constraints:
            feature['constraint'] = {
                'type': constraints[el['name']]['constraint'],
                'value': constraints[el['name']]['value']
            }
        elif el.name == 'attribute':
            feature['type'] = utils.convert_datatype(el['type'])
            
            try:
                del feature['features']
                del feature['attributes']
                del feature['group_type']
                del feature['mandatory']
            except KeyError:
                pass
        else:
            pass
    
    if parent_cardinality != "allof":
        try:
            del feature['mandatory']
        except KeyError:
            pass
    
    return feature