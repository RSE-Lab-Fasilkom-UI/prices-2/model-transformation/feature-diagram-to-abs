# Transform Feature Diagram to ABS Model

## Prerequisite
- Eclipse Oxygen and FeatureIDE Plugin to draw the feature diagram
- Python 3

Notes: Sample Feature diagram is provided within `aisco_feature_model.uml` file

## Installing Prerequisite
### Eclipse
- Download the program through https://featureide.github.io/#download
- Install

### Python
- Download Python programming language through https://www.python.org/downloads/
- Install
- All of the libraries requires are listed in `requirements.txt` file
    - Excecute `pip install -r requirements.txt` to install them
 
## How to use the program
- Draw the feature diagram in Eclipse
    - Look for the text representation of the diagram (By default, it is named as `model.xml`)
    - Get the file path to that `model.xml` file
- Execute `python main/main.py {path to the text representation file}`
- There will be 2 outputs
    - The dictionary produced by the parser will be inside `dictionary_output/` directory
    - The feature model written in ABS will be inside `output/` directory (**This is the main output of this program**)